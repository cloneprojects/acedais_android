package com.app.acedais.adapter;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.acedais.Service.ServiceClasss;
import com.app.acedais.activity.ChatActivity;
import com.app.acedais.activity.NewGroup_activity;
import com.app.acedais.activity.RequestForGroupActivity;
import com.app.acedais.baseUtils.SharedHelper;
import com.app.acedais.models.GroupParticiapntsModel;
import com.bumptech.glide.Glide;
import com.app.acedais.DBHelper.DBHandler;
import com.app.acedais.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

/*
 * Created by KrishnaDev on 1/5/17.
 */
public class RequestGroupAdapter extends RecyclerView.Adapter<RequestGroupAdapter.ViewHolder> {
    private JSONArray list;
    private Context myContext;

    public RequestGroupAdapter(Context activity, JSONArray jsonArray) {
        this.list = jsonArray;
        this.myContext = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(myContext).inflate(R.layout.request_group_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final DBHandler dbHandler = new DBHandler(myContext);
        final ServiceClasss.Emitters emitters = new ServiceClasss.Emitters(myContext);

        final JSONObject object = list.optJSONObject(holder.getAdapterPosition());
        // Log.e("tag", "onBindViewHolder: re" + object);
        final String admin_name = object.optString("fromName");
        final String group_name = object.optString("groupName");
        final String contact_image = object.optString("groupImage");
        final String participantId = object.optString("participantId");
        final String grp_id = object.optString("groupId");

        holder.user_name.setText(admin_name);
        holder.grp_name.setText(group_name);
        if (contact_image != null)
            Glide.with(myContext).load(contact_image).placeholder(R.drawable.ic_profile_group).into(holder.contact_image);


        holder.accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("id", "" + object.optString("_id"));
                    jsonObject.put("groupImage", "" + contact_image);
                    jsonObject.put("groupName", "" + group_name);
                    jsonObject.put("groupId", "" + grp_id);
                    //jsonObject.put("fromId", "" + object.optString("fromId"));
                    jsonObject.put("from", "" + object.optString("fromId"));
                    jsonObject.put("fromName", "" + admin_name);
                    jsonObject.put("participantId", "" + participantId);
                    jsonObject.put("participantName", "" + object.optString("participantName"));
                    //Log.e("tag", "groupRequest: " + jsonObject);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Boolean res = emitters.acceptGroupRequest(jsonObject);
                if (res) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        list.remove(holder.getAdapterPosition());

                    }
                    notifyItemRemoved(holder.getAdapterPosition());
                    Toast.makeText(myContext, R.string.added_group, Toast.LENGTH_LONG).show();

                }
            }
        });

        holder.reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final JSONObject jsonObject2 = new JSONObject();
                try {
                    jsonObject2.put("groupId", "" + grp_id);
                    jsonObject2.put("participantId", "" + participantId);
                    // Log.e("tag", "groupRequest: " + jsonObject2);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                emitters.rejectGroupRequest(jsonObject2);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    list.remove(holder.getAdapterPosition());

                }
                notifyItemRemoved(holder.getAdapterPosition());
                Toast.makeText(myContext, R.string.rejected, Toast.LENGTH_LONG).show();

            }
        });
        dbHandler.close();

    }

    @Override
    public int getItemCount() {
        return list.length();
    }

    public static int getPrimaryCOlor(Context context) {
        final TypedValue value = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.colorPrimary, value, true);
        return value.data;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView contact_image;
        private TextView user_name, grp_name;
        private ImageView accept, reject;


        public ViewHolder(View itemView) {
            super(itemView);
            contact_image = (CircleImageView) itemView.findViewById(R.id.contact_image);
            accept = (ImageView) itemView.findViewById(R.id.accept);
            reject = (ImageView) itemView.findViewById(R.id.reject);
            user_name = (TextView) itemView.findViewById(R.id.user_name);
            grp_name = (TextView) itemView.findViewById(R.id.grp_name);
        }
    }
}