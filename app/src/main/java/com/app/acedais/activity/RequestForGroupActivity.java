package com.app.acedais.activity;

import android.app.Activity;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.acedais.R;
import com.app.acedais.adapter.RequestGroupAdapter;
import com.app.acedais.baseUtils.AsyncTaskCompleteListener;
import com.app.acedais.baseUtils.Const;
import com.app.acedais.baseUtils.PostHelper;
import com.app.acedais.baseUtils.SharedHelper;
import com.app.acedais.baseUtils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RequestForGroupActivity extends AppCompatActivity {
    private RequestGroupAdapter requestGroupAdapter;
    private JSONArray jsonArray;
    private RecyclerView recyclerView;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_for_group);
        recyclerView = (RecyclerView) findViewById(R.id.requestGroupRv);
        textView = (TextView) findViewById(R.id.textView);
        ImageView back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(RequestForGroupActivity.this));
        new GroupRequest().execute();
    }

    private class GroupRequest extends AsyncTask<String, String, String> implements AsyncTaskCompleteListener {
        String result;

        @Override
        protected String doInBackground(String... strings) {
            JSONObject jsonObject = new JSONObject();

            try {
                jsonObject.put("userId", "" + SharedHelper.getKey(RequestForGroupActivity.this, "id"));
                Log.e("id", "doInBackground: " + SharedHelper.getKey(RequestForGroupActivity.this, "id"));
                boolean internet = Utils.isNetworkAvailable(RequestForGroupActivity.this);
                if (internet) {
                    new PostHelper(Const.Methods.GROUP_REQUEST, jsonObject.toString(),
                            Const.ServiceCode.GROUP_REQUEST, RequestForGroupActivity.this, this);
                } else {
                    result = "yes";
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        public void onTaskCompleted(JSONObject response, int serviceCode) {
            jsonArray = response.optJSONArray("requestList");
            Log.e("tag", "onTaskCompleted: " + jsonArray);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (jsonArray.length() != 0) {
                        textView.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        requestGroupAdapter = new RequestGroupAdapter(RequestForGroupActivity.this, jsonArray);
                        recyclerView.setAdapter(requestGroupAdapter);
                    } else {
                        textView.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                    }
                }
            });
        }
    }
}